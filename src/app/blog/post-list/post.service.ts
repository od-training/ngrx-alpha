import { Injectable } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { filter, map } from 'rxjs/operators';

import { State } from 'src/app/reducers';
import { selectCurrentPostId } from 'src/app/router.selectors';
import { PostDataService } from '../post/post-data.service';
import { Post } from '../types';
import { combineLatest } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class PostService {
  posts = this.postDataService.entities$;
  currentPostId = this.store.pipe(select(selectCurrentPostId));
  currentPost = combineLatest([
    this.posts,
    this.currentPostId.pipe(filter(postId => !isNaN(postId)))
  ]).pipe(
    map(([posts, postId]) => posts.find(post => post.id === postId))
  );

  constructor(
    private store: Store<State>,
    private postDataService: PostDataService
  ) {}

  getPostsByUser(userId: number | string) {
    return this.posts.pipe(
      map(posts => posts.filter(post => post.userId === +userId))
    );
  }

  createPost(post: Omit<Post, 'id'>) {
    // it's ok to exclude the id with a pessimistic save
    this.postDataService.add(post as Post, { isOptimistic: false });
  }

  deletePost(post: Post) {
    this.postDataService.delete(post);
  }

  updatePost(post: Post) {
    this.postDataService.update(post, { isOptimistic: false });
  }
}
