import { TestBed } from '@angular/core/testing';
import { provideMockStore } from '@ngrx/store/testing';
import { cold } from 'jasmine-marbles';
import { of } from 'rxjs';
import { take } from 'rxjs/operators';

import { mockPosts } from '../post/mock.posts';
import { PostDataService } from '../post/post-data.service';
import { PostService } from './post.service';

describe('PostService', () => {
  let service: PostService;
  let postDataMock: PostDataService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        provideMockStore({}),
        {
          provide: PostDataService,
          useValue: {
            entities$: of(mockPosts)
          }
        }
      ]
    });

    service = TestBed.inject(PostService);
    postDataMock = TestBed.inject(PostDataService);
  });

  describe('getPostsByUser', () => {
    it('should handle a number id', async () => {
      const result = await service
        .getPostsByUser(1)
        .pipe(take(1))
        .toPromise();

      expect(result).toEqual([mockPosts[0], mockPosts[1]]);
    });

    it('should handle a string id (using marbles)', () => {
      const expected = cold('(a|)', {
        a: [mockPosts[0], mockPosts[1]]
      });

      expect(service.getPostsByUser('1')).toBeObservable(expected);
    });
  });
});
