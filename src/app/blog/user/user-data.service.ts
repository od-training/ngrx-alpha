import { Injectable } from '@angular/core';
import {
  EntityCollectionServiceBase,
  EntityCollectionServiceElementsFactory
} from '@ngrx/data';
import { User } from '../types';

@Injectable({ providedIn: 'root' })
export class UserDataService extends EntityCollectionServiceBase<
  User
> {
  constructor(
    serviceElementsFactory: EntityCollectionServiceElementsFactory
  ) {
    super('User', serviceElementsFactory);
    // initialize the data
    this.load();
  }
}
