import { Injectable } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { map } from 'rxjs/operators';

import { State } from 'src/app/reducers';
import { selectCurrentUserId } from 'src/app/router.selectors';
import { UserDataService } from './user-data.service';

@Injectable({ providedIn: 'root' })
export class UserService {
  users = this.userDataService.entities$;
  currentUserId = this.store.pipe(select(selectCurrentUserId));
  currentUser = combineLatest([this.users, this.currentUserId]).pipe(
    map(([users, userId]) => users.find(post => post.id === userId))
  );

  constructor(
    private store: Store<State>,
    private userDataService: UserDataService
  ) {}
}
