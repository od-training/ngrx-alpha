import { Component } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

import { ConfigService } from './config.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  editing = false;
  title = new FormControl();
  title$: Observable<string>;

  constructor(private configService: ConfigService) {
    this.title$ = configService.title.pipe(
      tap(title => this.title.setValue(title))
    );
  }

  setTitle() {
    this.editing = false;
    this.configService.setTitle(this.title.value);
  }
}
