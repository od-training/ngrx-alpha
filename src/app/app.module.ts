import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
  DefaultDataServiceConfig,
  EntityDataModule,
  HttpUrlGenerator
} from '@ngrx/data';
import { EffectsModule } from '@ngrx/effects';
import { StoreRouterConnectingModule } from '@ngrx/router-store';
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import {
  HttpClientInMemoryWebApiModule,
  InMemoryDbService
} from 'angular-in-memory-web-api';

import { environment } from '../environments/environment';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { entityConfig } from './entity-metadata';
import { InMemoryDataService } from './in-memory-data.service';
import { MaterialDepsModule } from './material-deps.module';
import { PluralHttpUrlGenerator } from './plural-http-url-generator';
import { metaReducers, reducers } from './reducers';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MaterialDepsModule,
    HttpClientModule,
    // The HttpClientInMemoryWebApiModule module intercepts HTTP requests
    // and returns simulated server responses.
    HttpClientInMemoryWebApiModule.forRoot(InMemoryDataService, {
      dataEncapsulation: false,
      delay: 300,
      passThruUnknownUrl: true
    }),
    StoreModule.forRoot(reducers, {
      metaReducers
    }),
    EffectsModule.forRoot([]),
    StoreDevtoolsModule.instrument({
      maxAge: 25,
      logOnly: environment.production
    }),
    StoreRouterConnectingModule.forRoot(),
    EntityDataModule.forRoot(entityConfig)
  ],
  providers: [
    { provide: InMemoryDataService, useExisting: InMemoryDbService }, // our server serves employees at /api/employees for all calls,
    // but Data assumes some calls will be at /api/employee. The
    // generator tells Data to use plural always.
    { provide: HttpUrlGenerator, useClass: PluralHttpUrlGenerator },
    // The default root is 'api', but we need '/api'
    {
      provide: DefaultDataServiceConfig,
      useValue: {
        root: '/api'
      }
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
