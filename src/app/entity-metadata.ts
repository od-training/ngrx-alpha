import {
  EntityDataModuleConfig,
  EntityMetadataMap
} from '@ngrx/data';

const entityMetadata: EntityMetadataMap = {
  User: {},
  Post: {}
};

export const entityConfig: EntityDataModuleConfig = {
  entityMetadata
};
